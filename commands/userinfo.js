const moment = require("moment");
require("moment-duration-format");
exports.run = function(bot, msg) {
  let mentionedUser = msg.mentions.users.first() || msg.author;
  if (!mentionedUser) return bot.embed(msg, bot.hex, "Invalid user mention:", "Please mention a user to get info.");
  msg.guild.fetchMember(mentionedUser).then(m => {
    let fetchedGame = m.user.presence.game ? m.user.presence.game.name : "No game found.";
    bot.embed(msg, bot.hex, "Successfully fetched user:",
     `• Nickname: ${m.nickname || "No Nickname."}
     \n• Status: ${statusConvert[m.user.presence.status]} 
     \n• Playing: ${fetchedGame}
     \n• Highest Role: ${m.highestRole.name}
     \n• Joined at: ${moment.utc(m.joinedAt).format("dddd, MMMM Do YYYY, HH:mm:ss")} 
     \n• Created at: ${moment.utc(m.user.createdAt).format("dddd, MMMM Do YYYY, HH:mm:ss")}`);
  });
};

const statusConvert = {
  online: "Online",
  offline: "Offline",
  idle: "Idle",
  dnd: "Do not disturb"
};

exports.conf = {
  activated: true,
  aliases: ['ui'],
  permLevel: 0
};

exports.help = {
  name: 'userinfo',
  description: 'Gets user info from mentioned user or yourself.',
  usage: 'userinfo <mention/undefined>'
};