exports.run = function(bot, msg) {
  bot.embed(msg, bot.hex, "Fetching Discord latency:", `API Latency: ${Math.round(bot.ping)}ms`);
}

exports.conf = {
    activated: true,
    aliases: [],
    permLevel: 2
  };
  
  exports.help = {
    name: 'latency',
    description: 'Gets the API latency for Discord.',
    usage: 'latency'
  };