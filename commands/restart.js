exports.run = function(bot, msg) {
  bot.embed(msg, bot.hex, "Are you sure you want to reboot:", "Reply with cancel to abort the reboot, or the bot will auto abort in 30 seconds.");
  const validAnswers = ["yes", "y", "no", "n", "cancel"];
  const collector = msg.channel.createCollector(m => m.author.id === msg.author.id, {time:30000});

  collector.on("message", async collected => {
    const lower = collected.content.toLowerCase();
    if (lower === "cancel" || lower === "no" || lower === "n") {
      return collector.stop("abort");
    } else if (lower === "yes" || lower === "y") {
      return collector.stop("kill");
    }
    return bot.embed(msg, bot.hex, "Invalid command exception:", `Only \`${validAnswers.join("`, `")}\` are valid, please supply one of those.`);
  });

  collector.on("end", async (collected, reason) => {
    if (reason === "kill") {
      await msg.channel.send("Rebooting now...");
      await bot.destroy();
      process.exit();
    } else if (reason === "time") {
      return msg.channel.send("Reboot timed out.");
    } else if (reason === "abort") {
      return msg.channel.send("Aborting reboot.");
    }
  });
};

exports.conf = {
    activated: true,
    aliases: [],
    permLevel: 10
  };
  
  exports.help = {
    name: 'restart',
    description: 'Reboots the bot.',
    usage: 'restart'
  };